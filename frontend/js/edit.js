/**
    * @description      : 
    * @author           : 
    * @group            : 
    * @created          : 02/04/2023 - 14:55:37
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 02/04/2023
    * - Author          : 
    * - Modification    : 
**/

let elements = document.querySelectorAll(".post__header_btn_edit");
dump(elements);

for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener("click", handleUiAndTalkToPhp);
}

function handleUiAndTalkToPhp() {

    dump('Hoooo, u click one of the edit btns on the screen document');
    dump(this.id);

    const postId = 'post-' + this.id;
    const formId = 'form-' + this.id;
    // post-35
    // form-35
    // . , ->
    // let targetedPost = document->getElementById(postId);
    let targetedPost = document.getElementById(postId);
    let targetedForm = document.getElementById(formId);

    targetedForm.style.display = 'block';
    targetedPost.style.display = 'none';
}

function cancelEditing(btn) {

    dump('iam cancelling the editing ...');
    dump(btn.id);


    const postId = 'post-' + btn.id;
    const formId = 'form-' + btn.id;
    // post-35
    // form-35
    let targetedPost = document.getElementById(postId);
    let targetedForm = document.getElementById(formId);
    targetedPost.style.display = 'block';
    targetedForm.style.display = 'none';
}


/*
another example with arrow function as a callback
        const updatePostForm = document.querySelector('#update-post-form');
        updatePostForm.addEventListener('submit', (event) => {

            event.preventDefault();

            const postId = updatePostForm.dataset.postId;
            const title = updatePostForm.querySelector('#post-title').value;
            const content = updatePostForm.querySelector('#post-content').value;

            const xhr = new XMLHttpRequest();

            xhr.open('PUT', `/api/posts/${postId}`);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.onload = function() {
                if (xhr.status === 200) {
                    const updatedPost = JSON.parse(xhr.responseText);
                    const postTitleElement = document.querySelector(`#post-${postId} h2`);
                    const postContentElement = document.querySelector(`#post-${postId} p`);
                    postTitleElement.textContent = updatedPost.title;
                    postContentElement.textContent = updatedPost.content;
                }
            };

            xhr.send(JSON.stringify({
                title,
                content
            }));
        });


        */


function handleAjaxRequest(e, btn) {
    //  determine which from to fetch data from 
    e.preventDefault();

    let formId = 'post-form-' + btn.getAttribute('our_submit_id');
    // data here
    let formEl = document.forms[formId];

    let formData = new FormData(formEl);


    let title = formData.get('title');
    let content = formData.get('content');
    let post_img = formData.get('post_img');
    dump(title);
    dump(content);
    dump(post_img);

    /* Data which will be sent to server */




    /*


    // callback
    function success() {
        var data = JSON.parse(this.responseText);
        // taget post.title 
        // target.text = text
        // image = image 

        dump(data);
        let postTitleId = 'post-title-' + data.post_id;
        let postTextId = 'post-content-' + data.post_id;

        document.getElementById(postTitleId).innerHTML = data.title;
        document.getElementById(postTextId).innerHTML = data.content;
        //  close form 
        // appear form 

        let targetFormId = 'form-' + data.post_id;
        console.log(targetFormId);
        let targetPostId = 'post-' + data.post_id;
        console.log(targetPostId);
        let form = document.getElementById(targetFormId);
        console.log(form);
        let post = document.getElementById(targetPostId);
        form.style.display = "none";
        post.style.display = "block";
        // apear post

        console.log(data);
    }

    */


    /*
    function error(err) {
        console.log('Error Occurred :', err);
    }

    */


    // const formIdParts = formId.split('-');
    // const postId = formIdParts[formIdParts.length - 1];

    const postId = btn.getAttribute('our_submit_id');

    dump(postId);


    // send data to server with ajax

    // let postObj = {
    //     title: title,
    //     content: content
    // }
    // let post = JSON.stringify(postObj)

    /*
    var xhr = new XMLHttpRequest();
    xhr.onload = success;
    xhr.onerror = error;
    xhr.open('POST', 'http://localhost/project_g2_posts/edit.php?post_id=' + postId, true);
    // xhr.setRequestHeader('Content-type', 'application/json; charset=UTF-8')
    xhr.send(formData);
    xhr.onreadystatechange = function() {
        dump(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == 200) {
            dump(xhr.responseText);
            // document.getElementById("result").innerHTML = xhr.responseText;
        }
    }

    */

    //  with fetch 

    /*

    ,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

    */

    // promise
    fetch('http://localhost/project_g2_posts/public/edit.php?post_id=' + postId, {
        method: 'post',
        body: formData
    }).then((response) => {
        return response.json()
    }).then((data) => {
        dump(data);

        let postTitleId = 'post-title-' + data.post_id;
        let postTextId = 'post-content-' + data.post_id;
        let postImageId = 'article_post__img-' + data.post_id;




        var output = document.getElementById(postImageId);
        output.src = data['full_post_image'];
        // output.src = URL.createObjectURL(event.target.files[0]);


        document.getElementById(postTitleId).innerHTML = data.title;
        document.getElementById(postTextId).innerHTML = data.content;
        //  close form 
        // appear form 

        let targetFormId = 'form-' + data.post_id;
        console.log(targetFormId);
        let targetPostId = 'post-' + data.post_id;
        console.log(targetPostId);
        let form = document.getElementById(targetFormId);
        console.log(form);
        let post = document.getElementById(targetPostId);
        form.style.display = "none";
        post.style.display = "block";
        // apear post

        console.log(data);

        if (res.status === 201) {
            console.log("Post UPDATED created!")
        }
    }).catch((error) => {
        console.log(error)
    })


}
