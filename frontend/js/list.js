/**
    * @description      : 
    * @author           : 
    * @group            : 
    * @created          : 05/04/2023 - 15:30:32
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 05/04/2023
    * - Author          : 
    * - Modification    : 
**/

getPostsFromServerAndDrawThemToTheDom();

function getPostsFromServerAndDrawThemToTheDom() {


    //Our native php with composer
    // const appDomain = 'http://localhost/it_gate_last/php-native-backend-api/public';
    // const listUrl = `${appDomain}/list.php`;
    // Laravel 
    // const appDomain = 'http://localhost/it_gate_last/php-laravel-api/public';
    const appDomain = 'http://157.230.17.155/';
    const listUrl = `${appDomain}api/post/list`;

    // const listUrl = 'http://localhost/it_gate_last/php-laravel-api/public/api/post/list';

    function success() {
        var data = JSON.parse(this.responseText);
        setTimeout(function () {
            document.getElementById("posts-container").innerHTML = '';
            handleDomDrawing(data)
        }, 2000);
        dump(data);
    }

    function error(err) {
        console.log('Error Occurred :', err);
    }

    var xhr = new XMLHttpRequest();
    xhr.onload = success;
    xhr.onerror = error;
    xhr.open('GET', listUrl, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    const token = window.localStorage.getItem('user_access_token');

    xhr.setRequestHeader('Authorization', `Bearer  ${token}`)
    xhr.send();
    xhr.onreadystatechange = function () {
        document.getElementById("posts-container").innerHTML = `
        <div class="post" style="justify-content: center;">
          <img src="https://i.gifer.com/VAyR.gif" >
        </div>
        `;
        dump(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == 200) {
            dump(xhr.responseText);
            // document.getElementById("posts-container").innerHTML = ``;
        }
    }
}


function handleDomDrawing(data) {
    const WWW_ROOT = '';
    // posts-container
    let postsContainer = document.getElementById("posts-container");
    for (const post of data) {
        let post_html = document.createElement("article");
        post_html.classList.add("post");
        post_html.innerHTML = formComponent(post);
        post_html.innerHTML += postComponent(post);
        postsContainer.appendChild(post_html);
    }

    function formComponent(post) {

        return `
                        <!-- Form  -->
                <section class="create-post" id="form-${post['post_id']}" style="display:none;">
                    <img class="create-post__avatar" src="${post['avatar_url']}" alt="" />

                    <form id="post-form-${post['post_id']}" class="create-post__form" action="" method="post" enctype="multipart/form-data" style=" border: black solid 5px;">
                        <button class="cancel_btn" id="${post['post_id']}" onclick="cancelEditing(this); return false;">Cancel</button>

                        <div class="create-post__text-wrap p-5">
                            <input type="text" class="p-5" title="" style="border: none;" name="title" value="${post['title']}" placeholder="Enter title...">
                        </div>

                        <div class="create-post__text-wrap p-5">
                            <textarea aria-label="Write something about you..." class="p-5" name="content" id="create-post-txt" oninput="this.parentNode.dataset.replicatedValue = this.value" placeholder="Write something about you...">
                        ${post['content']};
                </textarea>
                        </div>

                        <div class="post__body">
                            <img class="post__img" id="post__img-${post['post_id']}" src="${post['post_image']}" alt="" />
                        </div>

                        <div class="create-post__media-wrap" id="create-post-media-wrap"></div>

                        <div class="create-post__buttons">
                            <div class="create-post__assets-buttons">
                                <button type="button" aria-label="Add an image to the post" class="create-post__asset-btn" for="create-post-media" onclick="this.querySelector('input').click()">
                                    <img class="icon" src="https://raw.githubusercontent.com/Javieer57/create-post-component/43c8008a45b699957d2070cc23362f1953c65d78/icons/camera-tumblr.svg" alt="" />
                                    Photo
                                    <input type="file" our_photo_upload="${post['post_id']}" onchange="loadFile(event, this)" name="post_img" id="create-post-media" accept=".png, .jpg, .jpeg, .gif" />
                                </button>
                                <button type="button" aria-label="Add a video to the post" class="create-post__asset-btn" for="create-post-media" disabled>
                                    <img class="icon" src="https://raw.githubusercontent.com/Javieer57/create-post-component/43c8008a45b699957d2070cc23362f1953c65d78/icons/quote-tumblr.svg" alt="" />
                                    Quote
                                </button>
                                <button type="button" aria-label="Add a video to the post" class="create-post__asset-btn" for="create-post-media" disabled>
                                    <img class="icon" src="https://raw.githubusercontent.com/Javieer57/create-post-component/43c8008a45b699957d2070cc23362f1953c65d78/icons/link-tumblr.svg" alt="" />
                                    Link
                                </button>
                                <button type="button" aria-label="Add a video to the post" class="create-post__asset-btn" for="create-post-media" disabled>
                                    <img class="icon" src="https://raw.githubusercontent.com/Javieer57/create-post-component/43c8008a45b699957d2070cc23362f1953c65d78/icons/chat-tumblr.svg" alt="" />
                                    Chat
                                </button>
                                <button type="button" aria-label="Add a video to the post" class="create-post__asset-btn" for="create-post-media" disabled>
                                    <img class="icon" src="https://raw.githubusercontent.com/Javieer57/create-post-component/43c8008a45b699957d2070cc23362f1953c65d78/icons/audio-tumblr.svg" alt="" />
                                    Audio
                                </button>
                                <button type="button" aria-label="Add a video to the post" class="create-post__asset-btn" for="create-post-media" disabled>
                                    <img class="icon" src="https://raw.githubusercontent.com/Javieer57/create-post-component/43c8008a45b699957d2070cc23362f1953c65d78/icons/video-tumblr.svg" alt="" />
                                    Video
                                </button>
                            </div>
                            <button class="create-post__submit" our_submit_id="${post['post_id']}" onclick="handleAjaxRequest(event, this)" id="create-post-submit-btn">Publish</button>
                        </div>
                    </form>
                </section>
        
        `;

    }

    function postComponent(post) {

        return `
                        <div class="post" id="post-${post['post_id']}">
                    <img class="post__avatar" src="${post['avatar_url']}" alt="" />

                    <div class="post__content">
                        <header class="post__header">
                            <p class="post__user">${post['firstName']}</p>

                            <div class="post__meta">
                                <p class="post__reblogs">3,908</p>

                                <button class="post__header-btn">
                                    <img src="https://raw.githubusercontent.com/Javieer57/create-post-component/43c8008a45b699957d2070cc23362f1953c65d78/icons/reblog-tumblr.svg" alt="" />
                                </button>
                                <button class="post__header-btn">
                                    <img src="https://raw.githubusercontent.com/Javieer57/create-post-component/43c8008a45b699957d2070cc23362f1953c65d78/icons/heart-tumblr.svg" alt="" />
                                </button>




                                <button class="post__header_btn_edit" id="${post['post_id']}">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8ehfuvr7Ir5Ln9qyu_W29OID5PohuUJVepA&usqp=CAU" style="width: 22px; height: 20px;" alt="" />
                                </button>


                                <button class="post__header-btn" our_custom_post_id="${post['post_id']}" onclick="fireAjaxDeleteRequestToServer(event, this)">
                                    <img src="https://www.freeiconspng.com/thumbs/close-button-png/close-button-png-27.png" style="width: 22px; height: 20px;" alt="" />
                                </button>
                            </div>
                        </header>

                        <div class="post__body">
                            <img class="post__img" id="article_post__img-${post['post_id']}" src="${post['post_image']}" alt="" />
                            <!-- <a href="https://es.wikipedia.org/wiki/My_Life_as_Liz" class="post__text">My Life As Liz</a> -->
                            <h5 id="post-title-${post['post_id']}"> ${post['title']} </h5>
                            <p id="post-content-${post['post_id']}"> ${post['content']} </p>
                        </div>

                        <div class="post__footer">
                            <span>#2010s</span>
                            <span>#tumblr</span>
                            <span>#codepen</span>
                        </div>
                    </div>
                </div>
        
        `;

    }

    /*


        <!--POst  -->
    



    */

}