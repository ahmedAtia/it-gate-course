/**
    * @description      : 
    * @author           : 
    * @group            : 
    * @created          : 02/04/2023 - 14:53:00
    * 
    * MODIFICATION LOG
    * - Version         : 1.0.0
    * - Date            : 02/04/2023
    * - Author          : 
    * - Modification    : 
**/
function loadNewFile(event) {

    event.preventDefault();
    let output = document.getElementById('new__post__img');
    output.src = URL.createObjectURL(event.target.files[0]);

    output.style.display = 'block';

    // output.onload = function() {
    //     URL.revokeObjectURL(output.src) // free memory
    // }
};

var loadFile = function (event, uploadButton) {

    const postId = uploadButton.getAttribute("our_photo_upload");
    var output = document.getElementById('post__img-' + postId);
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function () {
        URL.revokeObjectURL(output.src) // free memory
    }
};
