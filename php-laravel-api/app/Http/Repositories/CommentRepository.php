<?php

namespace App\Http\Repositories;

use App\Models\Comment;

class CommentRepository
{


    public function exists($id)
    {
        return Comment::where('id', $id)->exists();
    }

    public function create($request)
    {
        return  Comment::create([
            'post_id' => $request->post_id,
            'commentor_id' => $request->commentor_id,
            'parent_comment_id' => $request->parent_comment_id,
            'photo_url' => $request->photo_url,
            'content' => $request->content,
        ]);
    }

    // public function update($id, $request)
    // {
    //     $post = Post::find($id);
    //     $data = [];
    //     $data['title'] =  $request->get('title');
    //     $data['content'] =  $request->get('content');
    //     $data['post_image'] = $request->get('post_image');

    //     $post->update($data);
    //     return $post;
    // }

    // function delete($id)
    // {
    //     Post::destroy($id);
    // }
}
