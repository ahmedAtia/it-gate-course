<?php

namespace App\Http\Repositories;

use App\Models\Post;

class PostRepo
{
    public function queryAllPosts()
    {
        // model 
        // repository 
        return \DB::select('
        SELECT 
			*,
            posts.id as post_id 
            FROM posts
            left join users
                on posts.creator_id = users.id
            ORDER BY posts.id DESC;
    ');
    }


    public function exists($id)
    {

        return Post::where('id', $id)->exists();
    }

    public function update($id, $request)
    {
        $post = Post::find($id);
        $data = [];
        $data['title'] =  $request->get('title');
        $data['content'] =  $request->get('content');
        $data['post_image'] = $request->get('post_image');

        $post->update($data);
        return $post;
    }

    function delete($id)
    {
        Post::destroy($id);
    }
}
