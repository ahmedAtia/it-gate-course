<?php

namespace App\Http\Services;

use App\Http\Repositories\CommentRepository;

class CommentService
{
    protected $commentRepository;
    public function __construct()
    {
        $this->commentRepository = new CommentRepository();
    }

    public function create($request)
    {
        return $this->commentRepository->create($request);
    }
    // public function getAllPosts()
    // {
    //     return $this->postRepo->queryAllPosts();
    // }

    // function delete($id)
    // {
    //     if ($this->postRepo->exists($id)) {
    //         return $this->postRepo->delete($id);
    //     }

    //     throw new \Exception('Model not found');
    // }

    // public function update($id, $request)
    // {

    //     return $this->postRepo->update($id, $request);
    // }
}
