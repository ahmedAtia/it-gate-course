<?php

namespace App\Http\Services;

use App\Http\Repositories\PostRepo;

class PostService
{
    protected $postRepo;
    public function __construct()
    {
        $this->postRepo = new PostRepo();
    }
    public function getAllPosts()
    {
        return $this->postRepo->queryAllPosts();
    }

    function delete($id)
    {
        if ($this->postRepo->exists($id)) {
            return $this->postRepo->delete($id);
        }

        throw new \Exception('Model not found');
    }

    public function update($id, $request)
    {

        return $this->postRepo->update($id, $request);
    }
}
