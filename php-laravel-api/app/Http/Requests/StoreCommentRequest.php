<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class StoreCommentRequest extends FormRequest
{
    public function rules()

    {
        return [
            'post_id' => 'required|exists:posts,id',
            'commentor_id' => 'required|exists:users,id',
            'parent_comment_id' => 'required|exists:comments,id',
            'photo_url' => 'sometimes',
            'content' => 'required',
        ];
    }



    public function failedValidation(Validator $validator)
    {

        throw new HttpResponseException(response()->json([

            'success'   => false,

            'message'   => 'Validation errors',

            'data'      => $validator->errors()

        ]));
    }



    public function messages()

    {
        return [];
        // return [
        //     'post_id.required' => 'Title is required',
        //     'body.required' => 'Body is required'
        // ];
    }
}
