<?php

namespace App\Http\Controllers;

use App\Http\Services\PostService;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $postService;
    public function __construct()
    {
        $this->postService = new PostService();
    }
    public function listPosts()
    {
        $posts = $this->postService->getAllPosts();
        return response()->json($posts);
    }


    public function editPost($id, Request $request)
    {
        $post = $this->postService->update($id, $request);
        return response()->json($post->refresh());
    }


    function deletePost(Request $request, $id)
    {
        try {
            $post = $this->postService->delete($id);
            return response()->json([
                "post $id deleted sucessfully"
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                "Model not found "
            ], 400);
        }
    }
}
