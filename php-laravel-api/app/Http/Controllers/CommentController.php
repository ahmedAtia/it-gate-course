<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Services\CommentService;
use App\Http\Requests\StoreCommentRequest;

class CommentController extends Controller
{
    protected $commentService;

    public function __construct()
    {
        $this->commentService = new CommentService();
    }

    public function createComment(StoreCommentRequest $request)
    {

        // $validation = Validator::make($request->all(), [
        //     'post_id' => 'required|exists:posts,id',
        //     'commentor_id' => 'required|exists:users,id',
        //     'parent_comment_id' => 'required|exists:comments,id',
        //     'photo_url' => 'sometimes',
        //     'content' => 'required',
        // ]);

        // if ($validation->fails()) {
        //     return response([
        //         $validation->errors()
        //     ], 400);
        // }
        $comment = $this->commentService->create($request);
        return response($comment);
    }

    public function getAllComments()
    {
    }

    public function showComment()
    {
    }

    public function editComment()
    {
    }


    public function deleteComment()
    {
    }

    public function getCommentsByPost()
    {
    }


    public function getCommentsByUser()
    {
    }
}
