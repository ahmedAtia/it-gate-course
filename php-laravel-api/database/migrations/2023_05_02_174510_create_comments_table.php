<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('post_id')->nullable();
            $table->unsignedBigInteger('commentor_id')->nullable();
            $table->unsignedBigInteger('parent_comment_id')->nullable();
            $table->string('photo_url')->nullable();
            $table->text('content')->nullable();

            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreign('commentor_id')->references('id')->on('users');
            $table->foreign('parent_comment_id')->references('id')->on('comments');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('comments');
    }
};
