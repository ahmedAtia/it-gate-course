<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Comment>
 */
class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'post_id' => Post::inRandomOrder()->first()->id,
            'commentor_id' => User::inRandomOrder()->first()->id,
            'parent_comment_id' => null,
            'photo_url' => fake()->imageUrl(),
            'content' => fake()->sentence(),
        ];
    }



    /**
     * Indicate that the model's email address should be unverified.
     */
    public function asReplies(): static
    {
        return $this->state(fn (array $attributes) => [
            'parent_comment_id' => Comment::inRandomOrder()->first()->id,
        ]);
    }
}
