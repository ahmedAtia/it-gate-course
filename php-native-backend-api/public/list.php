<?php
require_once('../app/initialize.php');

use \Myapp\Feeds\models\Post as myPost;
use \Myapp\Feeds\facebook\Post as facebookPost;

$post = new myPost($db);
$facebookPost = new facebookPost();

// dd($facebookPost);

if ($request->isPost()) {

	if ($request->file('post_img')) {
		$data = [];
		$data['title'] =  $request->get('title');
		$data['content'] =  $request->get('content');
		$data['post_img'] =  $request->move('post_img');
		$newPost = $post->create($data);
	}
}



$posts = $post->all();


$db->disconnect();




$response->json($posts);
