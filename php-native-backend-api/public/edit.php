<?php

require_once('../app/initialize.php');

use \app\models\Post;

/*
This guide demonstrates the five fundamental steps
of database interaction using PHP.
oop vs functional vs procedural
procedural
functional
OOP programming
*/

// $db.connect();
//  -> php === .  any other programming lang 


$post = new Post($db);


// old data
$postId = $request->get('post_id');

$postData = $post->findById($postId);

if ($request->isPost()) {



	$data = [];
	$data['title'] =  $request->get('title') ? $request->get('title') : $postData['title'];
	$data['content'] =  $request->get('content') ?  $request->get('content') :  $postData['content'];
	$data['post_img'] = $request->file('post_img') ?  $request->move('post_img') : $postData['post_image'];


	$postData = $post->update($request->get('post_id'), $data);
	$postData['full_post_image'] = "./public/" . $postData['post_image'];

	$response->json($postData);
	// redirect_to('./index.php?#' . $postData['post_id']);
}



$db->disconnect();
