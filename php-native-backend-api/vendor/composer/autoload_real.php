<?php

// autoload_real.php @generated by Composer

class ComposerAutoloaderInitdd7b6e6c36b71cc4c6b1f163e498d8cc
{
    private static $loader;

    public static function loadClassLoader($class)
    {
        if ('Composer\Autoload\ClassLoader' === $class) {
            require __DIR__ . '/ClassLoader.php';
        }
    }

    /**
     * @return \Composer\Autoload\ClassLoader
     */
    public static function getLoader()
    {
        if (null !== self::$loader) {
            return self::$loader;
        }

        require __DIR__ . '/platform_check.php';

        spl_autoload_register(array('ComposerAutoloaderInitdd7b6e6c36b71cc4c6b1f163e498d8cc', 'loadClassLoader'), true, true);
        self::$loader = $loader = new \Composer\Autoload\ClassLoader(\dirname(__DIR__));
        spl_autoload_unregister(array('ComposerAutoloaderInitdd7b6e6c36b71cc4c6b1f163e498d8cc', 'loadClassLoader'));

        require __DIR__ . '/autoload_static.php';
        call_user_func(\Composer\Autoload\ComposerStaticInitdd7b6e6c36b71cc4c6b1f163e498d8cc::getInitializer($loader));

        $loader->register(true);

        return $loader;
    }
}
