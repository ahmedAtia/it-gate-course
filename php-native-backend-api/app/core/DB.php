<?php

namespace Myapp\Feeds\core;

require_once(PROJECT_PATH . '/config/database.php');


class DB
{


    private $connection = null;
    private $result_set = null;

    // encapsulation
    private $data = [];



    // static functions 
    // construtor
    function __construct()
    {
        $this->connect();
    }
    function connect()
    {


        $dbhost = 'localhost';
        $dbname = 'feeds_db';
        $dbuser = 'root';
        $dbpass = 'root';

        // 1. Create a database connection
        $this->connection = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

        // Test if connection succeeded
        if (mysqli_connect_errno()) {
            $msg = "Database connection failed: ";
            $msg .= mysqli_connect_error();
            $msg .= " (" . mysqli_connect_errno() . ")";
            exit($msg);
        }

        // return $connection;
    }


    function query($sql)
    {

        // SELECT [ ] result_set
        // INSERT true | False
        // UPDATE true | False
        // DELETE true | False
        $this->result_set =  mysqli_query($this->connection, $sql);
        $this->confirm();
        return $this;
    }

    static function insert()
    {
        dump('hi here from static method called insert ');
    }

    function fetchOne()
    {
        return mysqli_fetch_assoc($this->result_set);
    }

    function fetch()
    {
        // 3. Use returned data (if any)
        while ($item = mysqli_fetch_assoc($this->result_set)) {
            // dump($subject);
            array_push($this->data, $item);
        }

        // object method chaining
        return $this;
    }

    function disconnect()
    {
        if (is_array($this->result_set)) {
            mysqli_free_result($this->result_set);
        }
        mysqli_close($this->connection);
    }


    function confirm()
    {
        if (!$this->result_set) {
            exit("Database query failed.");
        }
    }


    // getter method 
    // encapsulation
    public function get($key = null)
    {
        // key not exist => no data fond [] | return all | return [0]
        if (!isset($this->data[$key])  && !is_null($key)) {
            return [];
        }

        // key exists => return [key] 
        // 0 | null
        if (!is_null($key)) {
            // if ($key == null) {
            return $this->data[$key];
        }

        // not key => return all 
        return $this->data;
    }


    public function first()
    {
        return $this->data[0];
    }
}
