<?php

namespace Myapp\Feeds\core;


class Request
{
    function get($key)
    {
        return $_REQUEST[$key] ?? null;
    }

    function file($key)
    {
        // null coalescing operator php
        // return $_FILES[$key] ?? null;

        // ternary operator php
        return (isset($_FILES[$key])  && $_FILES[$key]['error'] == 0) ? true : false;
    }

    function upload($fileKey)
    {
        $filename = $_FILES[$fileKey]["name"];
        $tempname = $_FILES[$fileKey]["tmp_name"];

        $folder = "images/" . $filename;

        // Now let's move the uploaded image into the folder: image
        if (move_uploaded_file($tempname, public_path() . $folder)) {
            return $folder;
        } else {
            return null;
        }
    }

    function move($key)
    {
        return $this->upload($key);
    }

    function isPost()
    {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }
}
