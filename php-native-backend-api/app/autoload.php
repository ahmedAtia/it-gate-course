<?php

spl_autoload_register('myAutoload');
function  myAutoload($className)
{
    $className = str_replace('\\', '/', $className);
    require_once(PROJECT_PATH . "/$className.php");
    // the above line replace all the required statement for following lines ....
    // require_once(APP_PATH . '/Database.php');
    // require_once(APP_PATH . '/Request.php');
    // require_once(APP_PATH . '/Response.php');
}
