<?php

namespace Myapp\Feeds\models;

class Post
{

    private $db = null;
    function __construct($dbDriver)
    {
        $this->db = $dbDriver;
    }
    function all()
    {

        $this->db->query("

        SELECT 
			*,
            posts.id as post_id 
            FROM posts
            left join users
                on posts.creator_id = users.id
            ORDER BY posts.id DESC;
 
        ");

        return $this->db->fetch()->get();
    }




    function create($data)
    {

        $title = $data['title'];
        $content = $data['content'];
        $image = $data['post_img'];

        $sql = "

        INSERT INTO `posts` (`title`, `content`, `post_image`, `creator_id`) 
        VALUES ('$title', '$content', '$image', 2);
        ";

        $this->db->query($sql);
    }

    function findById($postId)
    {

        $sql = "
        SELECT 
			*,
            posts.id as post_id 
            FROM posts
            left join users
                on posts.creator_id = users.id
            WHERE posts.id = $postId 
        
            ";


        return $this->db->query($sql)->fetchOne();
    }

    function update($postId, $data)
    {

        $title = $data['title'];
        $content = $data['content'];
        $image = $data['post_img'];

        $sql = "
        UPDATE `posts` SET 
        `content` = '$content',
        `title` = '$title',
        `post_image` = '$image'
        WHERE (`id` = '$postId');
        ";


        $this->db->query($sql);

        return $this->findById($postId);
    }

    // 


    function delete($id)
    {
        $sql = "DELETE FROM `posts` WHERE (`id` = '$id');";
        $this->db->query($sql);
    }

    public function first()
    {

        return $this->db->get(0);
    }

    public function last()
    {
        $len = sizeof($this->db->get());
        return $this->db->get($len - 1);
    }
    public function before_last()
    {
        $len = sizeof($this->db->get());
        return $this->db->get($len - 2);
    }
}
